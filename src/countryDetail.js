
const imgContainer = document.getElementById('imgContainer');
const countryDetail = document.getElementById('countryDetail');
const countryImage = document.getElementById('countryImage');
const countryName = document.getElementById('countryName');
const nativeName = document.getElementById('nativeName');
const topDomain = document.getElementById('topDomain');
const population = document.getElementById('population');
const currencies = document.getElementById('currencies');
const region = document.getElementById('region');
const languages = document.getElementById('languages');
const subRegion = document.getElementById('subRegion');
const capital = document.getElementById('capital');
const borderCountries = document.getElementById('borderCountries');
const backButton = document.getElementById('backButton');

let borderCountriesObject = {};

function showLoader(){
    loader.style.display = "block";
    loader.classList += "mx-auto";
}

function hideLoader(){
    loader.style.display = "none";
}

async function fetchCountriesData() {
    const response = await fetch("https://restcountries.com/v3.1/all");
    return await response.json();
}

function countryDetailCard(country , countriesData) {

    countryImage.setAttribute('src', country['flags']['png']);

    countryName.innerText = country['name']['common'];

    const nativeNameKeys = Object.keys(country['name']['nativeName']);
    console.log(country['name']['nativeName'][nativeNameKeys[0]]);
    nativeName.textContent = `Native Name : ${country['name']['nativeName'][nativeNameKeys[0]]['common']}`;

    topDomain.textContent = `Top Level Domain : ${country['tld'][0]}`

    population.textContent = `Population : ${country['population']}`;

    const currenciesKeys = Object.keys(country['currencies']);
    currencies.textContent = `Currencies : ${country['currencies'][currenciesKeys[0]]['name']}`;

    region.textContent = `Region : ${country['region']}`;

    const languagesKeys = Object.keys(country['languages']);
    languages.textContent = 'Languages : ' ;

    for (let key of languagesKeys) {
        languages.textContent += `${country['languages'][key]} ,` ;
    }

    subRegion.textContent = `Sub Region : ${country['subregion']}` ;

    capital.textContent = `Capital : ${country['capital']}` ;

    const borders = country['borders'] ;
    console.log(borders)
    borderCountries.innerHTML = " Border Countries : " ;

    borders.forEach((borderCode)=>{

        const indexOfBorderCountry = borderCountriesObject[borderCode] ;
        const borderButton = document.createElement('button') ;
        borderButton.classList += "border-2 border-gray-200 mx-2 mt-0.5 p-1" ;
        borderButton.textContent = countriesData[indexOfBorderCountry]['name']['common'] ;

        borderButton.addEventListener('click' , ()=>{
            countryDetailCard(countriesData[indexOfBorderCountry] , countriesData);
        })

        borderCountries.appendChild(borderButton) ;
    });

}

function getCountryDetail() {

    showLoader();

    fetchCountriesData().then((countriesData) => {

        const urlParams = new URLSearchParams(window.location.search);
        const index = urlParams.get("index").split(';')[0];

        const size = countriesData.length;

        for (let index = 0; index < size; index++) {

            borderCountriesObject[countriesData[index]['cca3']] = index;
        }

        countryDetailCard(countriesData[index] , countriesData);

    }).catch((err)=>{
        console.log(err) ;
    }).finally(()=>{
        hideLoader();
    });

}

function goBacktoHome(){
    backButton.addEventListener('click' , ()=>{
        window.location.href = 'index.html'
    })
}

goBacktoHome() ;

getCountryDetail();