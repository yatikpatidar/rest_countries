const homePage = document.getElementById('homePage');
const searchCountry = document.getElementById('searchCountry');
const filterRegion = document.getElementById('filterRegion');
const loader = document.getElementById('loader');
// const countryDetail = document.getElementById('countryDetail');

function showLoader() {
    loader.style.display = "block";
    loader.classList += "mx-auto";
}
function hideLoader() {
    loader.style.display = "none";
}


async function fetchCountriesData() {

    const response = await fetch("https://restcountries.com/v3.1/all");
    return await response.json();
}

var regionDataObject = {
    'Default': []
}

function appendCountry(country, index) {

    const countryInfo = document.createElement('div');
    countryInfo.classList += "bg-white w-[250px] my-4";

    countryInfo.addEventListener('click', () => {
        redirectToCountryPage(index);
    });

    const countryImage = document.createElement('img');
    countryImage.classList += "max-w-[250px] h-[180px]";
    countryImage.src = country['flags']['png'];

    const countryName = document.createElement("h2");
    countryName.classList += "text-[20px] px-4 py-6";
    countryName.textContent = country['name']['common'];

    const countryPopulation = document.createElement('p');
    countryPopulation.classList += "px-4 pb-2";
    countryPopulation.textContent = `Population : ${country['population']}`;

    const countryRegion = document.createElement('p');
    countryRegion.classList += "px-4 pb-2";
    countryRegion.textContent = `Region : ${country['region']}`;

    const countryCapital = document.createElement('p');
    countryCapital.classList += "px-4 pb-16";
    countryCapital.textContent = `Capital : ${country['capital']}`;


    countryInfo.appendChild(countryImage);
    countryInfo.appendChild(countryName);
    countryInfo.appendChild(countryPopulation);
    countryInfo.appendChild(countryRegion);
    countryInfo.appendChild(countryCapital);
    homePage.appendChild(countryInfo);
}

function redirectToCountryPage(index) {
    window.location.href = `aboutCountry.html?index=${index};`;

}

function startFun() {
    showLoader();
    fetchCountriesData().then((countriesData) => {

        const size = countriesData.length;

        for (let index = 0; index < size; index++) {
            const region = countriesData[index]['region'];

            regionDataObject['Default'].push(index);

            if (regionDataObject[region] === undefined) {
                regionDataObject[region] = [index];
            } else {
                regionDataObject[region].push(index);

            }
            appendCountry(countriesData[index], index);
        }

    }).catch((err) => {
        console.log(err);
    }).finally(() => {
        hideLoader();
    });
}

function filterCountriesByRegion() {
    filterRegion.addEventListener('change', () => {

        fetchCountriesData().then((countriesData) => {
            homePage.innerHTML = '';
            const inputCountry = searchCountry.value;
            const region = filterRegion.value;
            const regionWiseIndex = regionDataObject[region];

            for (let index of regionWiseIndex) {
                if (inputCountry != "") {
                    const countryName = countriesData[index]['name']['common'].toLowerCase();
                    if (countryName.includes(inputCountry.toLowerCase())) {
                        appendCountry(countriesData[index], index);
                    }
                }
                else {

                    appendCountry(countriesData[index], index);
                }
            }
        });
    });
}

function searchCountryByName() {
    searchCountry.addEventListener('input', (event) => {

        fetchCountriesData().then((countriesData) => {

            const inputCountry = searchCountry.value;
            const homePage = document.getElementById('homePage');
            homePage.innerHTML = '';

            const region = filterRegion.value;
            const regionWiseIndex = regionDataObject[region];
            for (let index of regionWiseIndex) {
                const countryName = countriesData[index]['name']['common'].toLowerCase();
                if (countryName.includes(inputCountry.toLowerCase())) {
                    appendCountry(countriesData[index], index);
                }
            }
        });
    })
}

startFun();

filterCountriesByRegion();

searchCountryByName();